from src.basic_api_call import multi_answers, PROMPT

for temperature in [0, 1, 2]:
    print(f"Temperature = {temperature}")
    chat_completion = multi_answers(temperature=temperature)
    print(PROMPT + " ...")
    for i in range(len(chat_completion.choices)):
        print("\t", chat_completion.choices[i].message.content)

# %%
