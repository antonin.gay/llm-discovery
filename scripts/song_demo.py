from src.basic_api_call import song_completion

chat_completions = song_completion(temperature=0)

for chat_completion in chat_completions:
    print(chat_completion.choices[0].message.content)

# The results are exactly the lyrics of the song
# %%
