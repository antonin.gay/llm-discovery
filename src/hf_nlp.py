from transformers import pipeline


def sentiment_analysis(text_list: list[str]) -> list[dict]:
    classifier = pipeline(
        task="sentiment-analysis",
        model="distilbert-base-uncased-finetuned-sst-2-english",
    )

    return classifier(text_list)


def sentiment_label(text_list: list[str]) -> list[dict]:
    classifier = pipeline(
        task="sentiment-analysis", model="SamLowe/roberta-base-go_emotions", top_k=5
    )

    return classifier(text_list)


def summarization(text: str) -> str:
    summarizer = pipeline("summarization", model="facebook/bart-large-cnn")
    summarized_text = summarizer(text, min_length=5, max_length=140)[0]["summary_text"]

    return summarized_text
