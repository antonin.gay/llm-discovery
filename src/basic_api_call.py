from openai import OpenAI
from openai.types.chat.chat_completion import ChatCompletion
from .constant import OPENAI_API_KEY

PROMPT = "Listen to your"

client = OpenAI(api_key=OPENAI_API_KEY)


def first_call() -> ChatCompletion:
    chat_completion = client.chat.completions.create(
        model="gpt-3.5-turbo",
        messages=[{"role": "user", "content": PROMPT}],
        max_tokens=1,
    )

    return chat_completion


def multi_answers(temperature: float) -> ChatCompletion:
    chat_completion = client.chat.completions.create(
        model="gpt-3.5-turbo",
        messages=[{"role": "user", "content": PROMPT}],
        max_tokens=2,
        n=5,
        temperature=temperature,
    )

    return chat_completion


def song_completion(temperature: float) -> list[ChatCompletion]:
    messages_list = [
        {
            "role": "system",
            "content": "I am Roxette lyric completion assistant. When given a line from a song, "
            "I will provide the next line in the song.",
        },
        {
            "role": "user",
            "content": "I know there's something in the wake of your smile",
        },
        {
            "role": "assistant",
            "content": "I get a notion from the look in your eyes, yeah",
        },
        {"role": "user", "content": "You've built a love but that love falls apart"},
        {"role": "assistant", "content": "Your little piece of Heaven turns too dark"},
        {"role": "user", "content": "Listen to your"},
    ]

    chat_completions: list[ChatCompletion] = []
    for _ in range(4):
        #     Generating next sentence
        chat_completion = client.chat.completions.create(
            model="gpt-3.5-turbo",
            messages=messages_list,
            max_tokens=15,
            n=1,
            temperature=temperature,
        )
        new_message = {
            "role": "assistant",
            "content": chat_completion.choices[0].message.content,
        }  # append new message to message list
        messages_list.append(new_message)

        chat_completions.append(chat_completion)

    return chat_completions
