# LLM discovery

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Ruff](https://img.shields.io/endpoint?url=https://raw.githubusercontent.com/astral-sh/ruff/main/assets/badge/v2.json)](https://github.com/astral-sh/ruff)


## Description

Following [this](https://towardsdatascience.com/cracking-open-the-openai-python-api-230e4cae7971) article, 
this project is made to discover the use of LLM in Python, from beginner to more advanced.  


## Formating

Applying those two lines from the root directory will ensure the code will pass the analysis step of CI/CD

```shell
ruff format .
black .
```